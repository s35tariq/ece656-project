create database animelist;
use animelist;

SET GLOBAL local_infile=1;

drop table if exists AnimeTitle; 
drop table if exists AnimeScore;
drop table if exists AnimeGenre;
drop table if exists AnimeProducer;
drop table if exists AnimeStudio;
drop table if exists AnimeStatus;
drop table if exists UserTable;
drop table if exists UserAnimeList;

select '----------------------------------------------------------------' as '';
select 'Create AnimeTitle' as '';
create table AnimeTitle (anime_id int primary key NOT NULL AUTO_INCREMENT,
						title varchar(50),
                        title_english varchar(50),
                        type varchar(15),
                        source varchar(15),
                        duration varchar(20),
                        rating varchar(15),
                        opening_theme varchar(50),
                        ending_theme varchar(50));
load data local infile 'D:\\University\\Second Term\\ECE 656\\Project\\archive\\anime_cleaned.csv' ignore into table animeTitle fields terminated by ',' enclosed by '"' lines terminated by '\n' IGNORE 1 LINES (anime_id, title, title_english, @dummy, @dummy, @dummy, type, source, @dummy, @dummy, @dummy, @dummy, @dummy, duration, rating, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy,@dummy, @dummy, @dummy, @dummy, @dummy, opening_theme, ending_theme);

select '----------------------------------------------------------------' as '';
select 'Create AnimeScore' as '';
create table animeScore (anime_id int,
						score decimal(10,2),
                        scored_by decimal(7),
                        `rank` decimal(7),
                        popularity decimal(7),
						foreign key (anime_id) references AnimeTitle(anime_id));
load data local infile 'D:\\University\\Second Term\\ECE 656\\Project\\archive\\anime_cleaned.csv' ignore into table animeScore fields terminated by ',' enclosed by '"' lines terminated by '\n' IGNORE 1 LINES (anime_id, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, score, scored_by, `rank`, popularity, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy);

select '----------------------------------------------------------------' as '';
select 'Create AnimeStatus' as '';
create table animeStatus (anime_id int,
							status varchar(20),
                            airing varchar(20),
                            aired_string varchar(20),
                            premiered varchar(20),
                            broadcast varchar(80),
                            aired_from_year decimal(4),
                            foreign key (anime_id) references AnimeTitle(anime_id));
load data local infile 'D:\\University\\Second Term\\ECE 656\\Project\\archive\\anime_cleaned.csv' ignore into table animeStatus fields terminated by ',' enclosed by '"' lines terminated by '\n' IGNORE 1 LINES (anime_id, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, status, airing, aired_string, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, premiered, broadcast, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, aired_from_year);

select '----------------------------------------------------------------' as '';
select 'Create AnimeProducer' as '';

create table animeProducerTemp (anime_id int,
								producer varchar(150));
load data local infile 'D:\\University\\Second Term\\ECE 656\\Project\\archive\\anime_cleaned.csv' ignore into table animeProducerTemp fields terminated by ',' enclosed by '"' lines terminated by '\n' IGNORE 1 LINES (anime_id, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, producer, @dummy, @dummy, @dummy, @dummy, @dummy);

CREATE TABLE ints(
	i int
);
INSERT INTO ints(i) VALUES (0), (1), (2), (3), (4), (5), (6), (7), (8), (9);
CREATE TABLE AnimeProducer AS SELECT DISTINCT anime_id,
							SUBSTRING_INDEX(SUBSTRING_INDEX(producer,',',i+1),',',-1) producer
                            FROM animeProducerTemp, ints ORDER BY anime_id, producer;
UPDATE AnimeProducer SET producer= TRIM(producer);
Alter table AnimeProducer ADD FOREIGN KEY (anime_id) REFERENCES AnimeTitle(anime_id);
drop table animeProducerTemp;
drop table ints;

select '----------------------------------------------------------------' as '';
select 'Create AnimeGenre' as '';

create table genreTableTemp (anime_id int,
								genre varchar(50));
load data local infile 'D:\\University\\Second Term\\ECE 656\\Project\\archive\\anime_cleaned.csv' ignore into table genreTableTemp fields terminated by ',' enclosed by '"' lines terminated by '\n' IGNORE 1 LINES ( anime_id, @dummy,  @dummy, @dummy, @dummy, @dummy,  @dummy,  @dummy, @dummy, @dummy, @dummy, @dummy, @dummy,  @dummy,  @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy,@dummy, @dummy, @dummy, @dummy,genre,  @dummy,  @dummy);
CREATE TABLE ints(
	i int
);
INSERT INTO ints(i) VALUES (0), (1), (2), (3), (4), (5), (6), (7), (8), (9);
CREATE TABLE AnimeGenre AS SELECT DISTINCT anime_id,
						SUBSTRING_INDEX(SUBSTRING_INDEX(genre,',',i+1),',',-1) genre
                        FROM genreTableTemp, ints ORDER BY anime_id, genre;
UPDATE AnimeGenre SET genre= TRIM(genre);
Alter table AnimeGenre ADD FOREIGN KEY (anime_id) REFERENCES AnimeTitle(anime_id);
drop table genreTableTemp;
drop table ints;

select '----------------------------------------------------------------' as '';
select 'Create AnimeStudio' as '';

create table animeStudio (anime_id int,
							studio varchar(50),
							foreign key (anime_id) references AnimeTitle(anime_id));
load data local infile 'D:\\University\\Second Term\\ECE 656\\Project\\archive\\anime_cleaned.csv'  ignore into table animeStudio fields terminated by ',' enclosed by '"' lines terminated by '\n' IGNORE 1 LINES ( anime_id, @dummy,  @dummy, @dummy, @dummy, @dummy,  @dummy,  @dummy, @dummy, @dummy, @dummy, @dummy, @dummy,  @dummy,  @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy,@dummy, @dummy, @dummy, studio , @dummy, @dummy,@dummy,@dummy,@dummy);                            


select '----------------------------------------------------------------' as '';
select 'Create UserAnimeList' as '';

create table UserAnimeList (username varchar(50),
							anime_id int,
                            my_watched_episodes int,
                            my_score int,
                            my_status int,
                            FOREIGN KEY (anime_id) REFERENCES AnimeTitle(anime_id));
load data local infile 'D:\\University\\Second Term\\ECE 656\\Project\\archive\\UserAnimeList.csv'  ignore into table UserAnimeList fields terminated by ',' enclosed by '"' lines terminated by '\n' IGNORE 1 LINES ( username, anime_id, my_watched_episodes, @dummy, @dummy, my_score, my_status, @dummy, @dummy, @dummy, @dummy);

select '----------------------------------------------------------------' as '';
select 'Create UserTable' as '';

create table UserTable(user_id decimal(7),
						username varchar(50),
                        user_watching decimal(7),
                        user_days_spent_watching decimal(10,5),
                        gender varchar(20),
                        location varchar(50),
                        Primary Key(user_id,username));
load data local infile 'D:\\University\\Second Term\\ECE 656\\Project\\archive\\users_cleaned.csv'  ignore into table UserTable fields terminated by ',' enclosed by '"' lines terminated by '\n' IGNORE 1 LINES ( username, user_id , user_watching, @dummy, @dummy, @dummy, @dummy, user_days_spent_watching, gender, location, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy);

INSERT INTO UserTable(user_id,username) VALUES (1,'admin');
INSERT INTO UserTable(user_id,username) VALUES (002,'studio');
DELETE FROM UserTable WHERE username = 'xinil';
