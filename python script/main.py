import mysql.connector

def admin_view(cursor):
    sql_select_Query2 = "show tables"
    cursor.execute(sql_select_Query2)
    # get all records
    records = cursor.fetchall()
    for row in records:
        print("Table Name = ", row[0])

    table_name = input("Enter Table Name to view: ")
    limit = input("limit: ")
    sql_select_Query3 = "select * from {} limit {}".format(table_name, limit)
    cursor.execute(sql_select_Query3)
    records = cursor.fetchall()
    print(cursor.column_names)
    for row in records:
        print(row)

def admin_insert(connection,cursor):
    print("\nEnter the Details below.\n")
    studio = input("Studio Name= ")
    producer = input("Producer= ")
    title = input("Name = ")
    title_english = input("Name in English  = ")
    type = input("Aired on  = ")
    source = input("Source  = ")
    duration = input("Duration  = ")
    rating = input("Rating = ")
    opening_theme = input("Opening Theme  = ")
    ending_theme = input("Ending Theme = ")
    genre = input("Genre= ")

    insert_query = "INSERT INTO animetitle(anime_id,title,title_english,type,source,duration,rating,opening_theme,ending_theme) VALUES(DEFAULT,%s, %s,%s, %s,%s, %s,%s, %s)"

    val = (title, title_english, type, source, duration, rating, opening_theme, ending_theme)
    cursor.execute(insert_query, val)
    connection.commit()

    insert_id = cursor.lastrowid

    insert_query2 = "INSERT INTO animegenre(anime_id,genre) VALUES({},'{}')".format(insert_id, genre)

    cursor.execute(insert_query2)
    connection.commit()

    insert_query3 = "INSERT INTO animestudio(anime_id,studio) VALUES({},'{}')".format(insert_id, studio)

    cursor.execute(insert_query3)
    connection.commit()

    insert_query4 = "INSERT INTO animeproducer(anime_id,producer) VALUES({},'{}')".format(insert_id, producer)

    cursor.execute(insert_query4)
    connection.commit()

    print(cursor.rowcount, "Records Inserted")

def admin_delete(connection,cursor):
    delete = input("Enter anime ID to Delete: ")
    delete_Query1 = "Delete from animegenre where anime_id = {}".format(delete)
    delete_Query2 = "Delete from animescore where anime_id = {}".format(delete)
    delete_Query3 = "Delete from animestatus where anime_id = {}".format(delete)
    delete_Query4 = "Delete from animeproducer where anime_id = {}".format(delete)
    delete_Query5 = "Delete from animestudio where anime_id = {}".format(delete)
    delete_Query6 = "Delete from useranimelist where anime_id = {}".format(delete)
    delete_Query7 = "Delete from animetitle where anime_id = {}".format(delete)

    cursor.execute(delete_Query1)
    connection.commit()
    cursor.execute(delete_Query2)
    connection.commit()
    cursor.execute(delete_Query3)
    connection.commit()
    cursor.execute(delete_Query4)
    connection.commit()
    cursor.execute(delete_Query5)
    connection.commit()
    cursor.execute(delete_Query6)
    connection.commit()
    cursor.execute(delete_Query7)
    connection.commit()

    print(cursor.rowcount, "records inserted")

def genreSearch(cursor,genre):
    print(genre)
    sql_select_Query = "SELECT * FROM animetitle INNER JOIN animegenre ON " \
                       "animetitle.anime_id = animegenre.anime_id Where " \
                       "animegenre.genre = '{}' ORDER BY RAND() limit 30".format(genre)
    cursor.execute(sql_select_Query)
    records = cursor.fetchall()
    for row in records:
        print("Name = ", row[1])
        print("Name in English  = ", row[2])
        print("Aired on  = ", row[3])
        print("Source  = ", row[4])
        print("Duration  = ", row[5])
        print("Rating = ", row[6])
        print("Opening Theme  = ", row[7])
        print("Ending Theme = ", row[8], "\n")

def user_rec(cursor,username,password):
    usertable_q = "select anime_id from usertable inner join useranimelist on " \
                  "usertable.username = useranimelist.username where " \
                  "usertable.username = '{}' and usertable.user_id={}".format(
                    username, password)

    cursor.execute(usertable_q)
    records = cursor.fetchall()
    genre_list = []
    for row in records:
        print(row[0])
        genre_q = "select genre from animegenre where anime_id = {}".format(row[0])
        cursor.execute(genre_q)
        records = cursor.fetchall()
        for row in records:
            genre_list.append(row[0])

    most_watched_genre = (max(((item, genre_list.count(item)) for item in set(genre_list)), key=lambda a: a[1])[0])

    recom_q = "SELECT * FROM animetitle INNER JOIN animegenre ON a" \
              "nimetitle.anime_id = animegenre.anime_id Where " \
              "animegenre.genre = '{}' ORDER BY RAND() limit 30".format(most_watched_genre)
    cursor.execute(recom_q)
    records = cursor.fetchall()
    for row in records:
        print("Name = ", row[1])
        print("Name in English  = ", row[2])
        print("Aired on  = ", row[3])
        print("Source  = ", row[4])
        print("Duration  = ", row[5])
        print("Rating = ", row[6])
        print("Opening Theme  = ", row[7])
        print("Ending Theme = ", row[8])
        print("Genre = ", row[10], "\n")

def user_view(connection,cursor,username,password):
    option = input("Select an Option.\n1.Find Anime By Name \n2.Find Anime By Genre \n3.Trending Now \n4.Highest Rated Anime \n5.Get Recommendations \nEnter Option: ")
    if(option == "1"):
        name = input("Enter Anime Name: ")
        sql_select_Query = "select * from AnimeTitle where Title like '%{}%'".format(name)
        cursor.execute(sql_select_Query)
        records = cursor.fetchall()
        for row in records:
            print("Name = ", row[1])
            print("Name in English  = ", row[2])
            print("Aired on  = ", row[3])
            print("Source  = ", row[4])
            print("Duration  = ", row[5])
            print("Rating = ", row[6])
            print("Opening Theme  = ", row[7])
            print("Ending Theme = ", row[8], "\n")

    if (option == "2"):
        genre_search = input(
            "Select Genre: \n 1.Action \n2.Adventure \n3.Comedy \n4.Drama\n5.Space\n6.Shounen \n7.Sci-Fi \n8.Mystery\n9.Slice of life\n10.Romance\n Enter Number: ")
        if (genre_search == "1"):
            genre = "Action"
            genreSearch(cursor, genre)
        if (genre_search == "2"):
            genre = "Adventure"
            genreSearch(cursor, genre)
        if (genre_search == "3"):
            genre = "Comedy"
            genreSearch(cursor, genre)
        if (genre_search == "4"):
            genre = "Drama"
            genreSearch(cursor, genre)
        if (genre_search == "5"):
            genre = "Space"
            genreSearch(cursor, genre)
        if (genre_search == "6"):
            genre = "Shounen"
            genreSearch(cursor, genre)
        if (genre_search == "7"):
            genre = "Sci-Fi"
            genreSearch(cursor, genre)
        if (genre_search == "8"):
            genre = "Mystery"
            genreSearch(cursor, genre)
        if (genre_search == "9"):
            genre = "Slice of Life"
            genreSearch(cursor, genre)
        if (genre_search == "10"):
            genre = "Romance"
            genreSearch(cursor, genre)

    if (option == "3"):
        sql_select_Query = "select * from animetitle inner join animestatus on animetitle.anime_id=animestatus.anime_id " \
                           "inner join animescore on animetitle.anime_id = animescore.anime_id where animestatus.airing = " \
                           "'True' order by animescore.score DESC,animescore.popularity DESC limit 50;"
        cursor.execute(sql_select_Query)
        records = cursor.fetchall()
        for row in records:
            print('title: ',row[1])
            print('title_english: ',row[2])
            print('type: ',row[3])
            print('source: ',row[4])
            print('duration: ',row[5])
            print('rating: ',row[6])
            print('opening_theme: ',row[7])
            print('ending_theme: ',row[8])
            print('status: ',row[9])
            print('airing: ',row[10])
            print('score: ',row[17])
            print('scored by: ',row[16])
            print('rank: ',row[18])
            print('popularity: ',row[19],"\n")

    if (option == "4"):
        sql_select_Query = "select * from animescore inner join animetitle on " \
                           "animetitle.anime_id = animescore.anime_id" \
                           " where scored_by > 10000 order by score desc limit 50;"
        cursor.execute(sql_select_Query)
        records = cursor.fetchall()
        for row in records:
            print('title',row[6])
            print('score',row[1])
            print('scored by',row[2])
            print('rank',row[3])
            print('popularity',row[4])
            print('type',row[8])
            print('source',[9])
            print('duration',row[10],"\n")

    if(option == "5"):
        user_rec(cursor,username,password)

def studio_insert(connection,cursor):
    print("\nEnter the Details of your Studio below to add to database.\n")
    studio = input("Studio Name= ")
    producer = input("Producer= ")

    print("\nEnter the Details of your anime below to add to database.\n")
    title = input("Name = ")
    title_english = input("Name in English  = ")
    type = input("Aired on  = ")
    source = input("Source  = ")
    duration = input("Duration  = ")
    rating = input("Rating = ")
    opening_theme = input("Opening Theme  = ")
    ending_theme = input("Ending Theme = ")

    genre = input("Genre= ")

    insert_query = "INSERT INTO animetitle(anime_id,title,title_english,type," \
                   "source,duration,rating,opening_theme,ending_theme) " \
                   "VALUES(DEFAULT,%s, %s,%s, %s,%s, %s,%s, %s)"

    val = (title, title_english, type, source, duration, rating, opening_theme, ending_theme)
    cursor.execute(insert_query, val)
    connection.commit()

    insert_id = cursor.lastrowid

    insert_query2 = "INSERT INTO animegenre(anime_id,genre) VALUES({},'{}')".format(insert_id,genre)

    cursor.execute(insert_query2)
    connection.commit()

    insert_query3 = "INSERT INTO animestudio(anime_id,studio) VALUES({},'{}')".format(insert_id, studio)

    cursor.execute(insert_query3)
    connection.commit()

    insert_query4 = "INSERT INTO animeproducer(anime_id,producer) VALUES({},'{}')".format(insert_id, producer)

    cursor.execute(insert_query4)
    connection.commit()

    print(cursor.rowcount,"Records Inserted")

def main():
    try:
        connection = mysql.connector.connect(host='localhost',
                                             database='animelist',
                                             user='root',
                                             password='root')
        cursor = connection.cursor()
        login_option = input("Login as.\n1.Admin\n2.Studio\n3.user\nEnter Option: ")
        if (login_option == '1'):
            username = input("Enter username: ")
            password = input("Enter password: ")

            sql_select_Query1 = "select * from usertable where username = '{}' and user_id={}".format(username,password)

            cursor.execute(sql_select_Query1)
            # get all records

            records = cursor.fetchall()

            if(records):
                print("Login Successfull\n")
                admin_option = input("1.View tables\n2.Add Data in to tables\n3.Delete Data\nEnter Option: ")
                if(admin_option == '1'):
                    admin_view(cursor)
                if (admin_option == '2'):
                    admin_insert(connection,cursor)
                if (admin_option == '3'):
                    admin_delete(connection,cursor)
            else:
                print("Login Failed")

        if (login_option == '2'):
            username = input("Enter username: ")
            password = input("Enter password: ")

            sql_select_Query1 = "select * from usertable where username = '{}' and user_id={}".format(username,password)

            cursor.execute(sql_select_Query1)
            # get all records

            records = cursor.fetchall()

            if(records):
                print("Login Successfull\n")
                studio_insert(connection,cursor)

        if (login_option == '3'):
            username = input("Enter username: ")
            password = input("Enter password: ")

            sql_select_Query1 = "select * from usertable where username = '{}' and user_id={}".format(username,password)

            cursor.execute(sql_select_Query1)
            # get all records
            records = cursor.fetchall()
            if(records):
                print("Login Successfull\n")
                user_view(connection,cursor,username,password)

    except mysql.connector.Error as e:
        print("Error reading data from MySQL table", e)
    finally:
        if connection.is_connected():
            connection.close()
            cursor.close()
            print("MySQL connection is closed")

if __name__ == "__main__":
   main()