from mock_db import MockDB
from mock import patch
import utils

class TestUtils(MockDB):

    def test_db_write(self):
        with self.mock_db_config:
            self.assertEqual(utils.db_write("""INSERT INTO `animetitle` (`anime_id`, `title`,
            `title_english`,
              `type` ,`source`, `duration`,`rating`,`opening_theme`,`ending_theme`) VALUES
                            ('1', 'death note','death note','TV','Manga','25 min','0',
                            'blah', 'blah')"""), False)
            self.assertEqual(utils.db_write("""INSERT INTO `animetitle` (`anime_id`, 
            `title`, `title_english`,
              `type` ,`source`, `duration`,`rating`,
              `opening_theme`,`ending_theme`) VALUES
                            ('3', 'fullmetal','fullmetal','TV',
                            'Manga','25 min','0','blah', 'blah')"""), True)
            self.assertEqual(utils.db_write("""DELETE FROM `animetitle` WHERE id='1' """), False)
            self.assertEqual(utils.db_write("""DELETE FROM `animetitle` WHERE id='3' """), False)